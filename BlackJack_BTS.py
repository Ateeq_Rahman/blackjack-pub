class Cards:

	def __init__(self, deck):

		self.deck = deck

	def __str__(self):

		return f"BLACKJACK Cards.Inc"

	def distribute(self):
		playercard1 = self.deck[-1]
		self.deck.remove(playercard1)

		dealercard1 = self.deck[-1]
		self.deck.remove(dealercard1)

		playercard2 = self.deck[-1]
		self.deck.remove(playercard2)

		dealercard2 = self.deck[-1]
		self.deck.remove(dealercard2)

		playersplit1 = playercard1.split()
		playersplit2 = playercard2.split()
		dealersplit1 = dealercard1.split()
		dealersplit2 = dealercard2.split()
	
		return playercard1, playercard2, dealercard1, dealercard2, playersplit1[0], playersplit2[0], dealersplit1[0], dealersplit2[0]

	def hit(self):
		playercard3 = self.deck[-1]
		self.deck.remove(playercard3)
		playersplit3 = playercard3.split()
		return playercard3, playersplit3[0]

	def stay(self):
		playercard3 = 0
		playersplit3 = 0
		return playercard3, playersplit3


class Blackjack:

	def __init__(self, playerval1, playerval2, playerval3, dealerval1, dealerval2, dict_deck, deck):
		self.playerval1 = playerval1
		self.playerval2 = playerval2
		self.playerval3 = playerval3
		self.dealerval1 = dealerval1
		self.dealerval2 = dealerval2
		self.deck = deck
		self.dict_deck = dict_deck

	def __str__(self):
		return "BlackJack"

	def dealer_condition(self):
		if dealerval1 + dealerval2 <= 16:
			dealerval3 = self.deck[-1]
			self.deck.remove(dealerval3)
			return dealerval3
		else:
			return 0

	def player_condition(self):
		if self.playerval1+playerval2+playerval3 > 21:
			print('\n\n\n BUST!!!')
		elif self.dict_deck[playerval1] + self.dict_deck[playerval2] + self.dict_deck[playerval3] == 21:
			print('\n\n\n You WIN!!!')


	
class Bank:

	def __init__(self, totalamount):
		self.totalamount = totalamount
	def __str__(self):
		return f'You have {self.totalamount}'
	def chips(self):
		total_chips = int((self.totalamount/10))
		totalamount -= (total_chips*10)
		return total_chips
